# Return list of domains for the capture file
from typing import Dict, IO
import dpkt
import datetime


def filter_domains_from_pcap_file_name(filename: str) -> Dict:
    """
    Opens existing file in the dir with cm
    Finds the domains in the cap file if they exist
    Returns a Dictionary with {timestamp: domains}
    """
    domains: Dict = {}
    with open(filename, "rb") as cap_file:

        buf_packet = dpkt.pcap.Reader(cap_file)

        for timestamp, buffer in buf_packet:

            # Try and get eth data
            try:
                eth = dpkt.ethernet.Ethernet(buffer)
            except:
                continue

            # Get the get IP from eth data
            try:
                ip = eth.data
            except:
                continue

            try:
                udp = ip.data
            except:
                continue

            # filter for dns traffic
            try:
                dns = dpkt.dns.DNS(udp.data)
            except:
                continue

            # if the dns exists check for the query description
            if dns:
                # check for query name
                for qname in dns.qd:
                    # this is done to reduce the random hits with _tcp and _udp requests
                    if "," not in str(qname.name) and "_" not in str(qname.name):
                        domains.update(
                            {
                                str(datetime.datetime.fromtimestamp(timestamp)): str(
                                    qname.name
                                )
                            }
                        )
    return domains
