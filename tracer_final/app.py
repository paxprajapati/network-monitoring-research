# Captures the network traffic and prints out the domains

from capture import packet_capture_with_temp_file, packet_capture_with_file_str
from filter_domains_from_pcap import (
    filter_domains_from_pcap_file_name,
)


def get_with_file():
    """
    Get packets and list of domains with the hardcoded file.
    The file is set to empty in each call of this function.
    """
    # set a new filename
    file_name = "scapy.cap"

    # capture packets
    packet_capture_with_file_str(packet_count=1024, filename=file_name)

    # get domains from capture file
    domains = filter_domains_from_pcap_file_name(filename=file_name)

    return domains


def get_with_temp_file():
    """
    Get file with the temporary file.
    File is deleted after this operation is complete.
    """
    # capture packets in temporary file
    tempfile = packet_capture_with_temp_file(packet_count=1024)

    # get the domains from the captured file.
    # .name is to get the name of NamedTemporaryFile
    domains = filter_domains_from_pcap_file_name(filename=tempfile.name)
    return domains


print(get_with_file())
