# Packet Analysis

Notes from the paper :

Packet Analysis for network forensics : A comprehensive survey

1. **Intro**

- Captured information from the traversing network flow:

  - Control Information
    - IP Addresses
    - Error detetction codes
    - Sequencing information
    - Payload (intended message)
    - data from the OSSI layer 2 (frame) or layer 4 (segement/datagram)

2. **Capturing and storing packets**

   - information between distinct system is facilatated by protocols, seet of rules for communication
   - network monitoring is done through protocol analyzers for packet analysis (often called pkt sniffer or network analyzers)
   - analysis of the captured raw packets reveal information about the content
   - we need a NIC or WNIC into promiscuous mode
   - controller can monitor all packets rather than just those thee nics programmed to receive
   - Some tools that do decent packet analysis:
   - BPF (Berkley Packet Filter)
   - filters and passes only those packets defined by constraints to the kernel, improves performance
   - eBPF(Extended BPF) is an enchancment with support for forward and backward jumps and loops, global data stores (maps) and supports aggregating event stats
   - Its easy to do on the Hub network because data goes through every port in the network, slightly difficult in the switched networks.
   - **Capturing data in switched networks**
   - port mirroring (port spanning)
     - for this, we need access to cli or web based interface of the switch on which target pc is located, and switch needs to support port mirroring, and have an empty port where we can plug our sniffer
   - hubbing out
     - for this we need physical access to the switch
   - using tap
     - this requires special hardware tool (ntap) to be connected to network
   - ARP cache poisoning (ARP spoofing)
     - requires information like IP and more infor from analyzer system, the target system, and the router
   - **Information network packets hold**

   - online user activity
     - websites visited and time spent on them
     - successful and unsucessful login attempts
     - credentials
     - illegal file downloads
   - raw data

     - individual frames
     - client server conversations
     - packet streams
     - flows
     - sessions

   - Defacto standard for the capture format :

   - **libpcap(pcap)**

     - binary format with nanosecond precision timestamps
     - format might vary on different implmentation
     - general structure : ![pcappkt](img/Fig1_libpkt.png)
     - the packet data contains at most the first N bytess of each packet ( N < 65,535)
       -the global headers has - magic number(identified file format version and byte order) - GMT offseet - timestamp precision - the maximum length of captured packets (in otets) - the data link type
     - the info is followed by zero or more records of captured packet data.
     - each packet starts with timestamp in seconds, timestamp in Useconds, and number of octets of packeet saved in file, actual length of packet

   - The sucessor of pcap: pcap Next Generation Capture File Format (pcapng).
   - doesn't just dump packets, but allows for saving range of data types with generic block format
   - the general structure of pcapng file. [pcapngpkt](img/Fig2_pcapngpkt.png)
   - developed by IETF (Internet Engineering Task Force)
   - Details of the block
     - Depends on the block type
     - block types
     - Section header blocks
     - interface descrption blocks
     - simple and enhanced packet blocks
     - name resolution blocks
     - interface stat blocks
       -systemd journal export blocks
       - decryption secrets blocks
       - more custom blocks
   - Another capture format: snoop capture format

   - defined in IETF RFC 1671
   - each file is an array of octets consisting of fixed length file header and one or more variable length packet records.
   - 64 bit identification pattern
   - 32 bit version number
   - 32 bit datalink type

   - RedBack Smartedge pcap format

   - designed for PowerPC based based NetBSD and intelligent packet forwarding linecards
   - based on circuits and extends pcap with information about protocols and circuits

   - _More but less known capture formats_
     - InfoVista 5View Capture
     - the IxCatapult (formerly DCT 2000)
     - trace.out file format
     - the Cisco Secure IDS iplog format
     - the Symbian OS btsnoop format
     - the TamoSoftCommView Format
     - the Endace ERF capture format
     - the EyeSDN USB S0/E1 ISDN trace format
     - HP-UX nettle trace
     - the K12 text file format
     - Microsoft Network Monitor format
     - the NA Sniffer format
     - the Network General Sniffer format
     - the Network Instruments Observer Format
     - the NetXray format
     - the Novell LANalyzer format
     - the PDML (Packet Description Markup Language) format
     - the NetScaler Trace format
     - the RADCOM WAN/LAN Analyzer format
     - the Shomiti/Finisar Surveyor format
     - the Sniffer Pro format
     - the Tektronix K12xx.rff5 format
     - Visual Networks UpTime traffic capture format

3. **Processing Network Packets and packet flow**
   - network carving : reconstruction of data and files from the network packet streams
   - packet analysis (network analysis with packet analysers/network carvers) is the primary traceback in network forensics
   - making sense of encrypted traffic is more challenging than that of nencrypted traffic
   - although encrypted network traffic can be classified using Machine Learning
   - Source: `https://www.sciencedirect.com/science/article/pii/S1319157814000561?via%3Dihub`
   - Packet sniffing is tapping into packet flows and even re-transmitted packets, with different TCP properties.

> > Different techniques of packet analysis

- **Deep Packet Inspection (DPI)**

  - goes beyond the packet header and analyzes the packet payload as well.
    - can detect
    - used to identify excessive levels of non-business traffic in enterprises such as social media use that need to be filtered or blocked
    - detect data streams
    - video traffic
    - encrypted BitTorrent traffic
    - malicious traffic
    - hosts behind the Carrier-Grade NATs
    - extract non routable IP addresses from peer lists obtained from crawling BT Distributed Hash Table(DHT)
    - forensic by design industrial systems
    - is is so good that it raises privacy concerns as it can be used for mass surveillance.
    - helps to control and shape traffic
    - comapanies like **NETSCOUT** and **Sandvine** provide DPI services for policy enforcing. Meaning, what this is absolutely possible to do at enterprise level.
    - Further reading:
      - DPI Good Video talk: `https://www.youtube.com/watch?v=SCkxGqENgeo`
        - Talks about the future of DPI
        - How will the HTTPS and network analysis be in the future
        - Details from the survey findinds and exploration of the newest trends and technologies for network forensics
        - Summary and Notes:
          - What is DPI?
          - uses packet properties to find information about the client/server communication
          - Video from the qosmos, sell DPI products and sensors.
          - Slide 1: [!Slide1](img/Vid_Notes_1.png)
          - Agenda
            - BaseLine DPI
            - Beyond DPI
            - New Frontiers
            - The OEM / DPI Partnership
      - DPI in IOT : `https://www.sciencedirect.com/science/article/abs/pii/S1084804519300815?via%3Dihub`
      - Software defined network
      - Software Defined Permeter (Alternative to VPN)
        - Acts as a link between user and application.
        - So it might be useufl in implementing monitoring network activities without implementing a network sniffer.

- Using AI in Packet Analysis
  - an ontology of packet analysis and monitoring is well defined, and opens opportunities for the automated processes.
  - PACO (Packet Centric Network Ontology) and PAO(Packet Analysis Ontology) can capture the semantics of the actual network packets
  - example : to decreasse the rate of false positives using Snort, researchers appplied ml to decode the packet data, classify the network packets. The algorithm uses SVM, decision tree, a combination of SVM and fuzzy logic, and optimized SVM with firefly to differentiate between legitimate and malicious traffic. Source : `https://www.sciencedirect.com/science/article/abs/pii/S0167739X17323178?via%3Dihub`
  - Already existing: PacketAI (`https://packetai.co`), but only does infrastruct monitoring, maybe useless for the use cases.
  - DPI with semi-supervised learning is suitable for classificying flows to identify audio, video, interactive data.
    - Intersting paper : `https://doi.org/10.1016/j.procs.2018.04.331`
