import time
import os
from filter_domains_from_pcap import filter_domains_from_pcap

file_name = "captured.cap"
packetlen = 1024

# capture sequence
capture_script = f"tshark -q -F pcap -c {packetlen} -w {file_name}"
os.system(capture_script)

# wait before the analysis step
time.sleep(0.5)

domains = filter_domains_from_pcap(filename = file_name)

print(domains)
