# Scapy Docs: Notes and important takeaways

[[_TOC_]]

## Intro

Scapy is all in one tool for network forensics. And, can replace a lot of the forensics applications and modules. It eats up very less amount of memory compared to other libraries. The module can replace some parts of even tshark, wireshark, and dpkt.

It provides the different model for packet capture than other tools and provide a layer of abstraction over a lot of networking concepts.

Scapy also has tools to capture, analyse, and inject packets.

PS: Scapy has its own DSL to model the networking abstractions.

## Fast Packet Design

Scapy doesn't require 'run out of shell' model like other libs and offers beautiful syntax. A lot of the network probing tools can be implemented in few lines of Scapy code.

## Probe once, interpret many

Network discovery is blackbox testing. So every little details matter. Scapy returns all the queries and responses from the stimli's so that we can interpret as many time as we want with a single probe.

[Scapy Requests](img/1_scapy_notes.png)

## Scrapy decodes, doesn't interpret

A problem with the common network probing tools is that they intepresent the result instead of only decoding and giving facts. Reporting something like 'Received a TCP reset on port 80' is not subject to interpretation errors. But saying something like 'Port 80 is closed' is an interpretation that may be right ost of the time but can cause more harm in understanding the real issue some times. For example - in some cases the packet may not be filtered by firewall but rather there was no host to forward hte packet to.

Interpreting results can help users who don't know what port scan is but it can also do more harm than good, as it injects bias into the results.

## Demo

### Installing `scapy`

```bash
pip3 install scapy
```

- github link: `https://github.com/secdev/scapy/`

> > Note : In Scapy v2 use `from scapy.all import *` instead of `from scapy import *`

> > Libpcap configuration: You can configure and set libpcap integration as you wish

```python
from scapy.config import conf
conf.use_pcap = True
```

### Projects with scapy

link: `https://scapy.net/awesome-scapy/`

## Usage

### Getting started

- Sample sniffer to print packets by sniffing

```python
from scapy.all import sniff

packets = sniff(count = 10)

print(packets)

```

More documentation: `https://scapy.readthedocs.io/`
